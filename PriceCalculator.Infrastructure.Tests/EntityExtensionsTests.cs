﻿using System;
using System.Linq;
using PriceCalculator.Infrastructure.Repositories;
using PriceCalculator.Infrastructure.Repositories.ProductOffers;
using PriceCalculator.Infrastructure.Repositories.Products;
using Xunit;

namespace PriceCalculator.Infrastructure.Tests
{
    public class EntityExtensionsTests
    {
        [Fact]
        public void ToProduct_NullProductEntity_ThrowsException()
        {
            // Arrange
            var productEntity = (ProductEntity)null;

            // Act // Assert
            Assert.Throws<ArgumentNullException>(() => productEntity.ToProduct());
        }

        [Fact]
        public void ToProduct_ValidProductEntity_ReturnsValidProduct()
        {
            // Arrange
            var productEntity = new ProductEntity
            {
                Id = "TestId",
                Name = "TestName",
                CurrentPrice = 1.23m
            };

            // Act
            var result = productEntity.ToProduct();

            // Assert
            Assert.Equal(productEntity.Id, result.Id);
            Assert.Equal(productEntity.Name, result.Name);
            Assert.Equal(productEntity.CurrentPrice, result.CurrentPrice);
            Assert.Equal(0, result.AppliedPrice);
        }

        [Fact]
        public void ToProductOffer_NullProductOfferEntity_ThrowsException()
        {
            // Arrange
            var productOfferEntity = (ProductOfferEntity)null;

            // Act // Assert
            Assert.Throws<ArgumentNullException>(() => productOfferEntity.ToProductOffer());
        }

        [Fact]
        public void ToProductOffer_ValidProductOfferEntity_ReturnsValidProductOffer()
        {
            // Arrange
            var productOfferEntity = new ProductOfferEntity
            {
                Id = "TestId",
                OfferType = "ItemDiscount",
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(5),
                Description = "TestDescription",
                Discount = 1.23m,
                DiscountType = "Percentage",
                TriggerQuantity = 99,
                TriggerProductId = "TestTriggerId",
                OfferProductId = "TestOfferId",
                OfferProductQuantityDiscounted = 123
            };

            // Act
            var result = productOfferEntity.ToProductOffer();

            // Assert
            Assert.Equal(productOfferEntity.Id, result.Id);
            Assert.Equal(productOfferEntity.OfferType, result.OfferType.ToString());
            Assert.Equal(productOfferEntity.StartDate, result.StartDate);
            Assert.Equal(productOfferEntity.EndDate, result.EndDate);
            Assert.Equal(productOfferEntity.Description, result.Description);
            Assert.Equal(productOfferEntity.Discount, result.Discount);
            Assert.Equal(productOfferEntity.DiscountType, result.DiscountType.ToString());
            Assert.Equal(productOfferEntity.TriggerQuantity, result.TriggerQuantity);
            Assert.Equal(productOfferEntity.TriggerProductId, result.TriggerProductId);
            Assert.Equal(productOfferEntity.OfferProductId, result.OfferProductId);
            Assert.Equal(productOfferEntity.OfferProductQuantityDiscounted, result.OfferProductQuantityDiscounted);
        }

        [Fact]
        public void ToProductOffer_InvalidProductOfferType_ThrowsException()
        {
            // Arrange
            var productOfferEntity = new ProductOfferEntity
            {
                Id = "TestId",
                OfferType = "InvalidType",
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(5),
                Description = "TestDescription",
                Discount = 1.23m,
                DiscountType = "Percentage",
                TriggerQuantity = 99,
                TriggerProductId = "TestTriggerId",
                OfferProductId = "TestOfferId",
                OfferProductQuantityDiscounted = 123
            };

            // Act // Assert
            Assert.Throws<ArgumentException>(() => productOfferEntity.ToProductOffer());
        }

        [Fact]
        public void ToProductOffer_InvalidDiscountType_ThrowsException()
        {
            // Arrange
            var productOfferEntity = new ProductOfferEntity
            {
                Id = "TestId",
                OfferType = "ItemDiscount",
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddDays(5),
                Description = "TestDescription",
                Discount = 1.23m,
                DiscountType = "InvalidType",
                TriggerQuantity = 99,
                TriggerProductId = "TestTriggerId",
                OfferProductId = "TestOfferId",
                OfferProductQuantityDiscounted = 123
            };

            // Act // Assert
            Assert.Throws<ArgumentException>(() => productOfferEntity.ToProductOffer());
        }
    }
}
