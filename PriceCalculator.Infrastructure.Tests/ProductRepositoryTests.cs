using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PriceCalculator.Infrastructure.Repositories.Products;
using Xunit;

namespace PriceCalculator.Infrastructure.Tests
{
    public class ProductRepositoryTests
    {
        [Fact]
        public async Task GetProductsByName_NullProductNames_ThrowsArgumentException()
        {
            // Arrange
            var sut = new ProductRepository();

            // Act // Assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => sut.GetProductsByName(null));
        }

        [Fact]
        public void GetProductsByName_EmptyProductNames_ReturnsEmptyProductList()
        {
            // Arrange
            var sut = new ProductRepository();

            // Act
            var result = sut.GetProductsByName(new List<string>()).Result.ToList();

            // Assert
            Assert.Empty(result);
        }

        [Fact]
        public void GetProductsByName_UnknownProductName_ReturnsEmptyProductList()
        {
            // Arrange
            var sut = new ProductRepository();
            var products = new List<string>
            {
                "___UNKNOWN_PRODUCT___"
            };

            // Act
            var result = sut.GetProductsByName(products).Result.ToList();

            // Assert
            Assert.Empty(result);
        }

        [Fact]
        public void GetProductsByName_SingleKnownProduct_ReturnsSingleProduct()
        {
            // Arrange
            var sut = new ProductRepository();
            var products = new List<string>
            {
                "Beans"
            };

            // Act
            var result = sut.GetProductsByName(products).Result.ToList();

            // Assert
            Assert.Single(result);
            Assert.Equal("PRODUCT1", result[0].Id);
        }

        [Fact]
        public void GetProductsByName_TwoKnownProducts_ReturnsBothProducts()
        {
            // Arrange
            var sut = new ProductRepository();
            var products = new List<string>
            {
                "Beans",
                "Bread"
            };

            // Act
            var result = sut.GetProductsByName(products).Result.ToList();

            // Assert
            Assert.Equal(2, result.Count);
            Assert.Equal("PRODUCT1", result[0].Id);
            Assert.Equal("PRODUCT2", result[1].Id);
        }

        [Fact]
        public void GetProductsByName_TwoKnownAndOneUnknownProducts_ReturnsBothProducts()
        {
            // Arrange
            var sut = new ProductRepository();
            var products = new List<string>
            {
                "Beans",
                "Bread",
                "___UNKOWN_PRODUCT___"
            };

            // Act
            var result = sut.GetProductsByName(products).Result.ToList();

            // Assert
            Assert.Equal(2, result.Count);
            Assert.Equal("PRODUCT1", result[0].Id);
            Assert.Equal("PRODUCT2", result[1].Id);
        }

        [Fact]
        public void GetProductsByName_DuplicateProduct_ReturnsSingleProducts()
        {
            // Arrange
            var sut = new ProductRepository();
            var products = new List<string>
            {
                "Beans",
                "Beans"
            };

            // Act
            var result = sut.GetProductsByName(products).Result.ToList();

            // Assert
            Assert.Single(result);
            Assert.Equal("PRODUCT1", result[0].Id);
        }
    }
}
