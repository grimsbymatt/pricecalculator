using System;
using System.Linq;
using PriceCalculator.Infrastructure.Repositories;
using PriceCalculator.Infrastructure.Repositories.ProductOffers;
using PriceCalculator.Infrastructure.Repositories.Products;
using Xunit;

namespace PriceCalculator.Infrastructure.Tests
{
    public class ProductOfferRepositoryTests
    {
        [Fact]
        public void ToProduct_NullProductEntity_ThrowsException()
        {
            // Arrange
            var productEntity = (ProductEntity) null;

            // Act // Assert
            Assert.Throws<ArgumentNullException>(() => productEntity.ToProduct());
        }

        [Fact]
        public void ToProduct_ValidProductEntity_ReturnsValidProduct()
        {
            // Arrange
            var productEntity = new ProductEntity
            {
                Id = "TestId",
                Name = "TestName",
                CurrentPrice = 1.23m
            };

            // Act
            var result = productEntity.ToProduct();

            // Assert
            Assert.Equal(productEntity.Id, result.Id);
            Assert.Equal(productEntity.Name, result.Name);
            Assert.Equal(productEntity.CurrentPrice, result.CurrentPrice);
            Assert.Equal(0, result.AppliedPrice);
        }

        [Fact]
        public void GetActiveOffers_SixDaysHence_ReturnsOffer2()
        {
            // Arrange
            var sut = new ProductOfferRepository();

            // Act
            var result = sut.GetActiveOffers(DateTime.Today.AddDays(6)).Result.ToList();

            // Assert
            Assert.Single(result);
            Assert.Equal("OFFER2", result[0].Id);
        }

        [Fact]
        public void GetActiveOffers_31DaysAgo_ReturnsNoOffers()
        {
            // Arrange
            var sut = new ProductOfferRepository();

            // Act
            var result = sut.GetActiveOffers(DateTime.Today.AddDays(-31)).Result.ToList();

            // Assert
            Assert.Empty(result);
        }
    }
}
