﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using NSubstitute;
using NSubstitute.Exceptions;
using PriceCalculator.Api.Services;
using PriceCalculator.Core.Repositories.ProductOffers;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;
using PriceCalculator.Core.Repositories.Products;

namespace PriceCalculator.Api.Tests
{
    public class BasketServiceTests
    {
        [Fact]
        public void GetBasketSummary_NullProductList_ReturnsEmptyBasketSummary()
        {
            // Arrange 
            var sut = new BasketService(null, null);

            // Act
            var result = sut.GetBasketSummary(null).Result;

            // Assert
            Assert.Equal(0, result.SubTotal);
            Assert.Equal(0, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_EmptyProductList_ReturnsEmptyBasketSummary()
        {
            // Arrange 
            var sut = new BasketService(null, null);

            // Act
            var result = sut.GetBasketSummary(new List<string>()).Result;

            // Assert
            Assert.Equal(0, result.SubTotal);
            Assert.Equal(0, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public async Task GetBasketSummary_UnknownProduct_ThrowsException()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);
            var productList = new List<string> { "___UnknownProduct___" };

            // Act // Assert
            await Assert.ThrowsAsync<ArgumentException>(() => sut.GetBasketSummary(productList));
        }

        [Fact]
        public void GetBasketSummary_SingleProductNoCurrentOffers_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);
            var productList = new List<string> { "Beans" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(0.65m, result.SubTotal);
            Assert.Equal(0.65m, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_SingleProductNoRelevantOffers_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);
            var productList = new List<string> { "Bread" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(0.8m, result.SubTotal);
            Assert.Equal(0.8m, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_DuplicateProductNoRelevantOffers_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);
            var productList = new List<string> { "Bread", "Bread" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(1.6m, result.SubTotal);
            Assert.Equal(1.6m, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_TwoProductsNoRelevantOffers_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Bread", "Milk" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(2.1m, result.SubTotal);
            Assert.Equal(2.1m, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_TwoProductsOneRelevantOfferNotTriggered_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Bread", "Beans" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(1.45m, result.SubTotal);
            Assert.Equal(1.45m, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_MultibuyOfferTriggeredButNoOfferProducts_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Beans", "Beans", "Milk" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(2.6m, result.SubTotal);
            Assert.Equal(2.6m, result.Total);
            Assert.Empty(result.AppliedOffers);
        }

        [Fact]
        public void GetBasketSummary_DiscountOfferTriggered_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Apples" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(1m, result.SubTotal);
            Assert.Equal(0.9m, result.Total);
            Assert.Single(result.AppliedOffers);
            Assert.Equal("Apples 10% off", result.AppliedOffers.ToList()[0].Description);
            Assert.Equal(0.1m, result.AppliedOffers.ToList()[0].Amount);
        }

        [Fact]
        public void GetBasketSummary_DiscountOfferTriggeredForTwoProducts_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Apples", "Apples" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(2m, result.SubTotal);
            Assert.Equal(1.8m, result.Total);
            Assert.Equal(2, result.AppliedOffers.Count);
            Assert.Equal("Apples 10% off", result.AppliedOffers.ToList()[0].Description);
            Assert.Equal(0.1m, result.AppliedOffers.ToList()[0].Amount);
            Assert.Equal("Apples 10% off", result.AppliedOffers.ToList()[1].Description);
            Assert.Equal(0.1m, result.AppliedOffers.ToList()[1].Amount);
        }

        [Fact]
        public void GetBasketSummary_DiscountOfferTriggeredForTwoProductsPlusOtherProduct_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Apples", "Apples", "Bread" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(2.8m, result.SubTotal);
            Assert.Equal(2.6m, result.Total);
            Assert.Equal(2, result.AppliedOffers.Count);
            Assert.Equal("Apples 10% off", result.AppliedOffers.ToList()[0].Description);
            Assert.Equal(0.1m, result.AppliedOffers.ToList()[0].Amount);
            Assert.Equal("Apples 10% off", result.AppliedOffers.ToList()[1].Description);
            Assert.Equal(0.1m, result.AppliedOffers.ToList()[1].Amount);
        }

        [Fact]
        public void GetBasketSummary_MultibuyOffer_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Beans", "Beans", "Bread" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(2.1m, result.SubTotal);
            Assert.Equal(1.7m, result.Total);
            Assert.Single(result.AppliedOffers);
            Assert.Equal("Buy 2 beans get loaf half price", result.AppliedOffers.ToList()[0].Description);
            Assert.Equal(0.4m, result.AppliedOffers.ToList()[0].Amount);
        }

        [Fact]
        public void GetBasketSummary_MultibuyAndDiscountOffer_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Beans", "Beans", "Bread", "Apples" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(3.1m, result.SubTotal);
            Assert.Equal(2.6m, result.Total);
            Assert.Equal(2, result.AppliedOffers.Count);
            Assert.Single(result.AppliedOffers.Where(a => a.Description == "Buy 2 beans get loaf half price"));
            Assert.Equal(0.4m, result.AppliedOffers.Single(a => a.Description == "Buy 2 beans get loaf half price").Amount);
            Assert.Single(result.AppliedOffers.Where(a => a.Description == "Apples 10% off"));
            Assert.Equal(0.1m, result.AppliedOffers.Single(a => a.Description == "Apples 10% off").Amount);
        }

        [Fact]
        public void GetBasketSummary_MultibuyAndDiscountOfferPlusOtherProduct_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Beans", "Beans", "Bread", "Apples", "Milk" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(4.4m, result.SubTotal);
            Assert.Equal(3.9m, result.Total);
            Assert.Equal(2, result.AppliedOffers.Count);
            Assert.Single(result.AppliedOffers.Where(a => a.Description == "Buy 2 beans get loaf half price"));
            Assert.Equal(0.4m, result.AppliedOffers.Single(a => a.Description == "Buy 2 beans get loaf half price").Amount);
            Assert.Single(result.AppliedOffers.Where(a => a.Description == "Apples 10% off"));
            Assert.Equal(0.1m, result.AppliedOffers.Single(a => a.Description == "Apples 10% off").Amount);
        }

        [Fact]
        public void GetBasketSummary_MultibuyOfferAppearsTwice_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Beans", "Beans", "Bread", "Beans", "Beans", "Bread" };

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(4.2m, result.SubTotal);
            Assert.Equal(3.4m, result.Total);
            Assert.Equal(2, result.AppliedOffers.Count);
            Assert.Equal("Buy 2 beans get loaf half price", result.AppliedOffers.ToList()[0].Description);
            Assert.Equal(0.4m, result.AppliedOffers.ToList()[0].Amount);
            Assert.Equal("Buy 2 beans get loaf half price", result.AppliedOffers.ToList()[1].Description);
            Assert.Equal(0.4m, result.AppliedOffers.ToList()[1].Amount);
        }

        [Fact]
        public void GetBasketSummary_MultibuyOfferAppearsTwiceButShouldNotApplyToAllOfferProducts_ReturnsCorrectBasketSummary()
        {
            // Arrange 
            SetupProductMock();
            SetupOfferMock(2);

            var sut = new BasketService(_productRepository, _offerRepository);

            var productList = new List<string> { "Beans", "Beans", "Bread", "Beans", "Beans", "Bread", "Bread" }; // Last bread should be full price

            // Act
            var result = sut.GetBasketSummary(productList).Result;

            // Assert
            Assert.Equal(5m, result.SubTotal);
            Assert.Equal(4.2m, result.Total);
            Assert.Equal(2, result.AppliedOffers.Count);
            Assert.Equal("Buy 2 beans get loaf half price", result.AppliedOffers.ToList()[0].Description);
            Assert.Equal(0.4m, result.AppliedOffers.ToList()[0].Amount);
            Assert.Equal("Buy 2 beans get loaf half price", result.AppliedOffers.ToList()[1].Description);
            Assert.Equal(0.4m, result.AppliedOffers.ToList()[1].Amount);
        }

        private static IProductRepository _productRepository;
        private static IProductOfferRepository _offerRepository;

        private static void SetupOfferMock(int numOffers)
        {
            _offerRepository = Substitute.For<IProductOfferRepository>();

            _offerRepository.GetActiveOffers(Arg.Any<DateTime>()).Returns(
                Task.Run(() => ProductOffers.Take(numOffers)));
        }

        private static void SetupProductMock()
        {
            _productRepository = Substitute.For<IProductRepository>();

            _productRepository.GetProductsByName(Arg.Any<IEnumerable<string>>()).Returns(
                args => Task.Run(() => Products.Where(p => ((IEnumerable<string>)args[0]).Contains(p.Name))));
        }

        private static List<Product> Products { get; } = new List<Product>
        {
            new Product { Id = "PRODUCT1", Name = "Beans", CurrentPrice = 0.65m },
            new Product { Id = "PRODUCT2", Name = "Bread", CurrentPrice = 0.80m },
            new Product { Id = "PRODUCT3", Name = "Milk", CurrentPrice = 1.30m },
            new Product { Id = "PRODUCT4", Name = "Apples", CurrentPrice = 1.00m },
        };

        private static List<ProductOffer> ProductOffers { get; } = new List<ProductOffer>
        {
            new ProductOffer
            {
                Id = "OFFER1",
                OfferType = ProductOfferType.ItemDiscount,
                StartDate = DateTime.Today.AddDays(-2),
                EndDate = DateTime.Today.AddDays(5),
                Description = "Apples 10% off",
                Discount = 0.1m,
                DiscountType = ProductOfferDiscountType.Percentage,
                TriggerQuantity = 1,
                TriggerProductId = "PRODUCT4",
                OfferProductId = null,
                OfferProductQuantityDiscounted = 0
            },

            new ProductOffer
            {
                Id = "OFFER2",
                OfferType = ProductOfferType.Multibuy,
                StartDate = DateTime.Today.AddDays(-30),
                EndDate = null,
                Description = "Buy 2 beans get loaf half price",
                Discount = 0.5m,
                DiscountType = ProductOfferDiscountType.Percentage,
                TriggerQuantity = 2,
                TriggerProductId = "PRODUCT1",
                OfferProductId = "PRODUCT2",
                OfferProductQuantityDiscounted = 1
            }
        };
    }
}
