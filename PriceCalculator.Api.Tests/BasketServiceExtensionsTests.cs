using System;
using PriceCalculator.Core.Repositories.ProductOffers;
using PriceCalculator.Api.Services;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;
using Xunit;

namespace PriceCalculator.Api.Tests
{
    public class BasketServiceExtensionsTests
    {
        [Fact]
        public void ApplyDiscount_NullOffer_ThrowsException()
        {
            // Arrange
            const decimal amount = 1.1m;

            // Act // Assert
            Assert.Throws<ArgumentNullException>(() => amount.ApplyDiscount(null));
        }

        [Fact]
        public void ApplyDiscount_PercentageOffer_CalculatesPercentage()
        {
            // Arrange
            const decimal amount = 1.1m;

            var offer = new ProductOffer
            {
                Discount = 0.5m,
                DiscountType = ProductOfferDiscountType.Percentage
            };

            // Act
            var result = amount.ApplyDiscount(offer);

            // Assert

            Assert.Equal(0.55m, result);
        }

        [Fact]
        public void ApplyDiscount_ValueOffer_CalculatesValue()
        {
            // Arrange
            const decimal amount = 1.1m;

            var offer = new ProductOffer
            {
                Discount = 0.5m,
                DiscountType = ProductOfferDiscountType.Value
            };

            // Act
            var result = amount.ApplyDiscount(offer);

            // Assert

            Assert.Equal(0.6m, result);
        }

        [Fact]
        public void ApplyDiscount_ValueOfferWouldBeNegative_ReturnsZero()
        {
            // Arrange
            const decimal amount = 0.4m;

            var offer = new ProductOffer
            {
                Discount = 0.5m,
                DiscountType = ProductOfferDiscountType.Value
            };

            // Act
            var result = amount.ApplyDiscount(offer);

            // Assert

            Assert.Equal(0, result);
        }
    }
}
