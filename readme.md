#PriceCalculator

To run this project:

1. Ensure .NET Core 2.1 SDK and runtime are installed
2. Clone this repository
3. Open PriceCalculator.sln in VS2017 and build solution
4. Set PriceCalculator.Api as startup project and hit Start (F5)
5. Publish PriceCalculator project (e.g. by navigating in command window to PriceCalculator project folder and running "dotnet publish -r win10-x64" - amend as appropriate for target platform)
6. Navigate to publish folder in command prompt (e.g. ...\PriceCalculator\bin\Debug\netcoreapp2.1\win10-x64\publish on win10)
7. Run 'PriceCalculator [args]', e.g. 'PriceCalculator Milk Bread Beans'