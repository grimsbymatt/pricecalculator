﻿namespace PriceCalculator.Models.Response
{
    public class AppliedOffer
    {
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}
