﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace PriceCalculator.Models.Response
{
    public class BasketSummary
    {
        public BasketSummary()
        {
            AppliedOffers = new List<AppliedOffer>();
        }

        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }
        public ICollection<AppliedOffer> AppliedOffers { get; set; }

        [JsonIgnore]
        public static BasketSummary EmptyBasketSummary { get; } = new BasketSummary
        {
            SubTotal = 0m,
            Total = 0m,
            AppliedOffers = new List<AppliedOffer>()
        };
    }
}
