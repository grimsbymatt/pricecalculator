﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PriceCalculator.Models.Response;

namespace PriceCalculator
{
    // This is a quick and dirty demo app!!!

    class Program
    {
        private static readonly string url = "https://localhost:44392/api/basketsummary";

        static void Main(string[] args)
        {
            if (args == null || args.Length == 0)
            {
                System.Console.WriteLine("Please specify products");
                return;
            }
            
            try
            {
                var basketSummary = GetBasketSummary(args).Result;

                System.Console.WriteLine($"Subtotal: {FormatCurrency(Math.Round(basketSummary.SubTotal, 2))}");

                if (basketSummary.AppliedOffers.Any())
                {
                    var offersApplied = basketSummary.AppliedOffers.Select(a => a.Description).Distinct();

                    foreach (var offer in offersApplied)
                    {
                        var offerTotal = basketSummary.AppliedOffers.Where(a => a.Description == offer).Sum(a => a.Amount);

                        System.Console.WriteLine($"{offer}: -{FormatCurrency(Math.Round(offerTotal, 2))}");
                    }
                }
                else
                {
                    System.Console.WriteLine("(No offers available)");
                }

                System.Console.WriteLine($"Total: {FormatCurrency(Math.Round(basketSummary.Total, 2))}");
            }
            catch (Exception e)
            {
                System.Console.WriteLine("Error encountered:");
                System.Console.WriteLine(e.Message);
            }
        }

        private static async Task<BasketSummary> GetBasketSummary(IEnumerable<string> products)
        {
            var queryString = $"products={string.Join('&', products)}";

            var fullUrl = $"{url}?{queryString}";

            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(fullUrl);

                if (response.IsSuccessStatusCode)
                {
                    var basketSummaryString = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<BasketSummary>(basketSummaryString);
                }

                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        private static string FormatCurrency(decimal value)
        {
            if (value < 1)
            {
                return (value * 100).ToString("####") + "p";
            }

            return string.Format(new CultureInfo("en-GB"), "{0:C}", value);
        }
    }
}
