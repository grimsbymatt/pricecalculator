﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PriceCalculator.Api.Services;
using PriceCalculator.Models.Response;

namespace PriceCalculator.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketSummaryController : ControllerBase
    {
        private readonly IBasketService _basketService;

        public BasketSummaryController(IBasketService basketService)
        {
            _basketService = basketService;
        }

        [HttpGet]
        public async Task<ActionResult<BasketSummary>> GetBasketSummary([FromQuery(Name = "products")] string[] products)
        {
            try
            {
                return await _basketService.GetBasketSummary(products);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
