﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PriceCalculator.Core.Repositories.ProductOffers;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;
using PriceCalculator.Core.Repositories.Products;
using PriceCalculator.Models.Response;

namespace PriceCalculator.Api.Services
{
    public class BasketService : IBasketService
    {
        private const string UnknownOfferTypeText = "Unknown offer type";
        private readonly IProductRepository _products;
        private readonly IProductOfferRepository _offers;

        public BasketService(IProductRepository products, IProductOfferRepository offers)
        {
            _products = products;
            _offers = offers;
        }

        public async Task<BasketSummary> GetBasketSummary(IEnumerable<string> requestedProducts)
        {
            if (requestedProducts == null ||
                !requestedProducts.Any())
            {
                return BasketSummary.EmptyBasketSummary;
            }

            var products = await _products.GetProductsByName(requestedProducts);
            var offers = await _offers.GetActiveOffers(DateTime.Today);

            // Generate Product object per product requested (including duplicates)
            var allProductsRequested = ParseProducts(requestedProducts, products);

            return CheckOffers(allProductsRequested, offers);
        }

        private IEnumerable<Product> ParseProducts(IEnumerable<string> requestedProducts, IEnumerable<Product> products)
        {
            var errorString = string.Empty;

            var allProducts = new List<Product>();
            var productsNotFound = new HashSet<string>();

            foreach (var requestedProduct in requestedProducts)
            {
                var product = products.FirstOrDefault(p => p.Name == requestedProduct);

                if (product == null)
                {
                    productsNotFound.Add(requestedProduct);
                    continue;
                }

                allProducts.Add(new Product
                {
                    Id = product.Id,
                    Name = product.Name,
                    CurrentPrice = product.CurrentPrice
                });
            }

            if (productsNotFound.Any())
            {
                foreach (var productNotFound in productsNotFound)
                {
                    errorString += $"Product not found: {productNotFound}";
                }

                throw new ArgumentException(errorString);
            }

            return allProducts;
        }

        private BasketSummary CheckOffers(IEnumerable<Product> products, IEnumerable<ProductOffer> offers)
        {
            var productsRemaining = products.ToList();
            var productsAlreadyProcessed = new List<Product>();
            
            var result = new BasketSummary();

            foreach (var offer in offers)
            {
                if (IsOfferApplies(offer, productsRemaining))
                {
                    ApplyOffer(offer, productsRemaining, productsAlreadyProcessed, result);
                }
            }

            if (productsRemaining.Any())
            {
                // Pass copy of remaining products collection as products to process
                // (this avoids errors on changing the collection mid-enumeration)
                ProcessProducts(productsRemaining.ToList(), productsRemaining, productsAlreadyProcessed, null, false, result);
            }

            result.SubTotal = productsAlreadyProcessed.Sum(p => p.CurrentPrice);
            result.Total = productsAlreadyProcessed.Sum(p => p.AppliedPrice);

            return result;
        }

        // Move specified products from remaining to processed collections, applying offer discount if applicable
        private static void ProcessProducts(IEnumerable<Product> productsToProcess, ICollection<Product> remainingProducts,
            ICollection<Product> processedProducts, ProductOffer offer, bool applyDiscount, BasketSummary basketSummary)
        {
            foreach (var product in productsToProcess)
            {
                product.AppliedPrice = applyDiscount ? product.CurrentPrice.ApplyDiscount(offer) : product.CurrentPrice;

                if (applyDiscount)
                {
                    basketSummary.AppliedOffers.Add(new AppliedOffer
                    {
                        Description = offer.Description,
                        Amount = product.CurrentPrice - product.AppliedPrice
                    });
                }

                processedProducts.Add(product);
                remainingProducts.Remove(product);
            }
        }

        private void ApplyOffer(ProductOffer offer, ICollection<Product> remainingProducts,
            ICollection<Product> appliedProducts, BasketSummary basketSummary)
        {
            switch (offer.OfferType)
            {
                case ProductOfferType.ItemDiscount:
                    var productsToDiscount = remainingProducts.Where(p => p.Id == offer.TriggerProductId).ToList();
                    ProcessProducts(productsToDiscount, remainingProducts, appliedProducts, offer, true, basketSummary);
                    break;

                case ProductOfferType.Multibuy:
                    // Can apply multibuy discount multiple times where multiple qualifying product amounts in product list
                    while (remainingProducts.Count(p => p.Id == offer.TriggerProductId) >= offer.TriggerQuantity &&
                           remainingProducts.Any(p => p.Id == offer.OfferProductId))
                    {
                        var offerProductsToDiscount = remainingProducts.Where(p => p.Id == offer.OfferProductId);

                        if (offer.OfferProductQuantityDiscounted != ProductOfferConstants.AllOfferProductsDiscounted)
                        {
                            offerProductsToDiscount =
                                offerProductsToDiscount.Take(offer.OfferProductQuantityDiscounted);
                        }

                        ProcessProducts(offerProductsToDiscount, remainingProducts, appliedProducts, offer, true, basketSummary);

                        var triggerProductsToRemove = remainingProducts.Where(p => p.Id == offer.TriggerProductId)
                            .Take(offer.TriggerQuantity).ToList();

                        ProcessProducts(triggerProductsToRemove, remainingProducts, appliedProducts, offer, false, basketSummary);
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static bool IsOfferApplies(ProductOffer offer, IEnumerable<Product> products)
        {
            switch (offer.OfferType)
            {
                case ProductOfferType.ItemDiscount:
                    return products.Any(p => offer.TriggerProductId == p.Id);

                case ProductOfferType.Multibuy:
                    return products.Count(p => offer.TriggerProductId == p.Id) >= offer.TriggerQuantity;

                default:
                    throw new ArgumentOutOfRangeException(UnknownOfferTypeText);
            }
        }
    }
}
