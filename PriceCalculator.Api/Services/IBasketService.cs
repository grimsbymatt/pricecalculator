﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PriceCalculator.Models.Response;

namespace PriceCalculator.Api.Services
{
    public interface IBasketService
    {
        Task<BasketSummary> GetBasketSummary(IEnumerable<string> requestedProducts);
    }
}
