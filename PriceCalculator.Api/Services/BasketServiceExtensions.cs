﻿using System;
using PriceCalculator.Core.Repositories.ProductOffers;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;

namespace PriceCalculator.Api.Services
{
    internal static class BasketServiceExtensions
    {
        private const string InvalidDiscountTypeText = "Invalid discount type";

        public static decimal ApplyDiscount(this decimal originalPrice, ProductOffer offer)
        {
            if (offer == null)
            {
                throw new ArgumentNullException(nameof(offer));
            }

            switch (offer.DiscountType)
            {
                case ProductOfferDiscountType.Percentage:
                    return originalPrice * (1 - offer.Discount);
                
                case ProductOfferDiscountType.Value:
                    return Math.Max(0m, originalPrice - offer.Discount);

                default:
                    throw new ArgumentOutOfRangeException(InvalidDiscountTypeText);
            }
        }
    }
}
