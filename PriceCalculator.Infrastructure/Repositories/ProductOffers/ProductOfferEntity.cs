﻿using System;
using System.Collections.Generic;

namespace PriceCalculator.Infrastructure.Repositories.ProductOffers
{
    public class ProductOfferEntity
    {
        public string Id { get; set; }
        public string OfferType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public decimal Discount { get; set; }
        public string DiscountType { get; set; }
        public int TriggerQuantity { get; set; }
        public string TriggerProductId { get; set; }
        public string OfferProductId { get; set; }
        public int OfferProductQuantityDiscounted { get; set; }
    }
}
