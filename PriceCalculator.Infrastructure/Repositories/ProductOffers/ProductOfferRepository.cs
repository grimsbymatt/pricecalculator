﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PriceCalculator.Core.Repositories.ProductOffers;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;

namespace PriceCalculator.Infrastructure.Repositories.ProductOffers
{
    public class ProductOfferRepository : IProductOfferRepository
    {
        private static List<ProductOfferEntity> ProductOfferEntities { get; } = new List<ProductOfferEntity>
        {
            new ProductOfferEntity
            {
                Id = "OFFER1",
                OfferType = ProductOfferType.ItemDiscount.ToString(),
                StartDate = DateTime.Today.AddDays(-2),
                EndDate = DateTime.Today.AddDays(5),
                Description = "Apples 10% off",
                Discount = 0.1m,
                DiscountType = ProductOfferDiscountType.Percentage.ToString(),
                TriggerQuantity = 1,
                TriggerProductId = "PRODUCT4",
                OfferProductId = null,
                OfferProductQuantityDiscounted = 0
            },

            new ProductOfferEntity
            {
                Id = "OFFER2",
                OfferType = ProductOfferType.Multibuy.ToString(),
                StartDate = DateTime.Today.AddDays(-30),
                EndDate = null,
                Description = "Buy 2 beans get loaf half price",
                Discount = 0.5m,
                DiscountType = ProductOfferDiscountType.Percentage.ToString(),
                TriggerQuantity = 2,
                TriggerProductId = "PRODUCT1",
                OfferProductId = "PRODUCT2",
                OfferProductQuantityDiscounted = 1
            }
        };

        public Task<IEnumerable<ProductOffer>> GetActiveOffers(DateTime dateActive)
        {
            var dateActiveDay = dateActive.Date;

            var offers = ProductOfferEntities.Where(p => dateActiveDay >= p.StartDate && (p.EndDate == null || dateActiveDay <= p.EndDate)).Select(p => p.ToProductOffer());

            return Task.Run(() => offers);
        }
    }
}
