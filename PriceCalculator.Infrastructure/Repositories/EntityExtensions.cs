﻿using System;
using PriceCalculator.Core.Repositories.ProductOffers;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;
using PriceCalculator.Core.Repositories.Products;
using PriceCalculator.Infrastructure.Repositories.ProductOffers;
using PriceCalculator.Infrastructure.Repositories.Products;

namespace PriceCalculator.Infrastructure.Repositories
{
    internal static class EntityExtensions
    {
        public static Product ToProduct(this ProductEntity productEntity)
        {
            if (productEntity == null)
            {
                throw new ArgumentNullException(nameof(productEntity));
            }

            return new Product
            {
                Id = productEntity.Id,
                Name = productEntity.Name,
                CurrentPrice = productEntity.CurrentPrice
            };
        }

        public static ProductOffer ToProductOffer(this ProductOfferEntity productOfferEntity)
        {
            if (productOfferEntity == null)
            {
                throw new ArgumentNullException(nameof(productOfferEntity));
            }

            return new ProductOffer
            {
                Id = productOfferEntity.Id,
                OfferType = (ProductOfferType)Enum.Parse(typeof(ProductOfferType), productOfferEntity.OfferType),
                StartDate = productOfferEntity.StartDate,
                EndDate = productOfferEntity.EndDate,
                Description = productOfferEntity.Description,
                Discount = productOfferEntity.Discount,
                DiscountType = (ProductOfferDiscountType) Enum.Parse(typeof(ProductOfferDiscountType), productOfferEntity.DiscountType),
                TriggerQuantity = productOfferEntity.TriggerQuantity,
                TriggerProductId = productOfferEntity.TriggerProductId,
                OfferProductId = productOfferEntity.OfferProductId,
                OfferProductQuantityDiscounted = productOfferEntity.OfferProductQuantityDiscounted
            };
        }
    }
}
