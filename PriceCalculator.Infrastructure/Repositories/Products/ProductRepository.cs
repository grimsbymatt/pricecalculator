﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using PriceCalculator.Core.Repositories.Products;

namespace PriceCalculator.Infrastructure.Repositories.Products
{
    public class ProductRepository : IProductRepository
    {
        private static List<ProductEntity> ProductEntities { get; } = new List<ProductEntity>
        {
            new ProductEntity { Id = "PRODUCT1", Name = "Beans", CurrentPrice = 0.65m },
            new ProductEntity { Id = "PRODUCT2", Name = "Bread", CurrentPrice = 0.80m },
            new ProductEntity { Id = "PRODUCT3", Name = "Milk", CurrentPrice = 1.30m },
            new ProductEntity { Id = "PRODUCT4", Name = "Apples", CurrentPrice = 1.00m },
        };

        public Task<IEnumerable<Product>> GetProductsByName(IEnumerable<string> productNames)
        {
            if (productNames == null)
            {
                throw new ArgumentNullException(nameof(productNames));
            }

            if (!productNames.Any())
            {
                return Task.Run(() => (IEnumerable<Product>) new List<Product>());
            }

            var products = ProductEntities.Where(p => productNames.Contains(p.Name)).Select(p => p.ToProduct());

            return Task.Run(() => products);
        }
    }
}
