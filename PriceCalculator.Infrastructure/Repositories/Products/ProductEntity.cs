﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PriceCalculator.Infrastructure.Repositories.Products
{
    internal class ProductEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal CurrentPrice { get; set; }
    }
}
