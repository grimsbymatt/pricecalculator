﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PriceCalculator.Core.Repositories.ProductOffers
{
    public interface IProductOfferRepository
    {
        Task<IEnumerable<ProductOffer>> GetActiveOffers(DateTime dateActive);
    }
}
