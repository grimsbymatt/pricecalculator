﻿namespace PriceCalculator.Core.Repositories.ProductOffers.Enums
{
    public enum ProductOfferDiscountType
    {
        Percentage, Value
    }
}
