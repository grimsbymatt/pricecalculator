﻿namespace PriceCalculator.Core.Repositories.ProductOffers.Enums
{
    public enum ProductOfferType
    {
        ItemDiscount, Multibuy
    }
}
