﻿using System;
using System.Collections.Generic;
using System.Text;
using PriceCalculator.Core.Repositories.ProductOffers.Enums;

namespace PriceCalculator.Core.Repositories.ProductOffers
{
    public class ProductOffer
    {
        public string Id { get; set; }
        public ProductOfferType OfferType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Description { get; set; }
        public decimal Discount { get; set; }
        public ProductOfferDiscountType DiscountType { get; set; }
        public int TriggerQuantity { get; set; }
        public string TriggerProductId { get; set; }
        public string OfferProductId { get; set; }
        public int OfferProductQuantityDiscounted { get; set; }
    }
}
