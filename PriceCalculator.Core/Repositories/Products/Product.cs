﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PriceCalculator.Core.Repositories.Products
{
    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal CurrentPrice{ get; set; }
        public decimal AppliedPrice { get; set; }
    }
}
