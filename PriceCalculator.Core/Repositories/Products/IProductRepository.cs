﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PriceCalculator.Core.Repositories.Products
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetProductsByName(IEnumerable<string> name);
    }
}
